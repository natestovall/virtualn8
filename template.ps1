#requires -Version 2

###############################################################################
#              _      _               _ _   _  ___                            #
#             (_)    | |             | | \ | |/ _ \                           #
#       __   ___ _ __| |_ _   _  __ _| |  \| | (_) | ___ ___  _ __ ___        #
#       \ \ / / | '__| __| | | |/ _` | | . ` |> _ < / __/ _ \| '_ ` _ \       #
#        \ V /| | |  | |_| |_| | (_| | | |\  | (_) | (_| (_) | | | | | |      #
#         \_/ |_|_|   \__|\__,_|\__,_|_|_| \_|\___(_)___\___/|_| |_| |_|      #
#          © Copyright 2015-2016 by Nate Stovall (nate@virtualN8.com)         #
# This software is released for copy, use, and modification under the GPLv3.  #
# Please refer to the licensing footer at the end of the file.                #
###############################################################################

<#
.SYNOPSIS
	Basic Detailed Full Examples
.DESCRIPTION
	Basic Full Detailed
.PARAMETER 
	Full Detailed Parameter <parameter name>
.EXAMPLE
	Detailed Full 
.NOTES
	Full
	Version:
	Author:
	Requirements:
	External Dependencies:
	Known Bugs:
	Future Improvements:
	License: This software is released for copy, use, and modification under the GPLv3.
	Copyright: © 2015-2016 nate@virtualN8.com
.LINK
	Basic Full
.INPUTTYPE
	Full
.RETURNVALUE
	Full
#>

#---------------------------------------------------------- PARAMETERS ---#####
#
[CmdletBinding()]
Param()
#
#-------------------------------------------------- PARAMETER CLEAN-UP ---#####
#
#
#----------------------------------------------------------- FUNCTIONS ---#####
#
Function Sample {
[CmdletBinding()]
Param(
	[Parameter(
		Mandatory=$TRUE, 
		Position=1,
		ParameterSetName="User",
		ValueFromPipeline=$TRUE,
		ValueFromPipelineByPropertyName=$TRUE, #The variable name must = the name of an object's property passed to the function.
		ValueFromRemainingArguments=$TRUE, # Gobbles the non-specified parameters here
		HelpMessage="Blah Blah Blah."
		)] #END Parameter
		[Alias("")]
		[AllowNull()]
		[AllowEmptyString()]
		[AllowEmptyCollection()]
		[ValidateCount(1,5)] # Accepts from 1-5 parameter values
		[ValidateLength(1,10)] # Accepts 1-10 charater string
		[ValidatePattern("[0-9][0-9][0-9][0-9]")] #regex expression
		[ValidateRange(0,10)]  # accepts number from 0 to 10
		[ValidateScript({$_ -ge (get-date)})]   # $_ to access variable
		[ValidateSet("Low", "Average", "High")]
		[ValidateNotNull()]
		[ValidateNotNullOrEmpty()]  # Cannot be $NULL or empty string ""
		[String] $Hostname=$(Throw "-Hostname must be specified."),
	[Parameter(Mandatory=$FALSE)]
		[Switch] $Force=$FALSE  # user just adds -force to parameter list
	) #END Param()
	Write-Verbose "$(Get-Date -DisplayHint DateTime)   $($MyInvocation.MyCommand.Name) started execution."
	Write-Verbose "$(Get-Date -DisplayHint DateTime)   $($MyInvocation.MyCommand.Name) finished execution."
} #END Function Sample
Function Add-PowerCLI {
#  © Copyright 2015-2016 by Nate Stovall (nate@virtualN8.com)
# This software is released for copy, use, and modification under the GPLv3.
	[CmdletBinding()]
	Param()
	Write-Verbose "$(Get-Date -DisplayHint DateTime)   $($MyInvocation.MyCommand.Name) started execution."
	If ((Get-PSSnapin -Registered) -like "*VimAuto*") { 
		Write-Verbose "$(Get-Date -DisplayHint DateTime)   PowerCLI is installed."
	} Else {
		Write-Verbose "$(Get-Date -DisplayHint DateTime)   PowerCLI not installed. Will attempt to install."
		Add-PSSnapin "Vmware.VimAutomation.Core"
		If ($? -eq $FALSE) {
			Throw "PowerCLI is not installed! Aborting script..."
		}
	}
	Write-Verbose "$(Get-Date -DisplayHint DateTime)   $($MyInvocation.MyCommand.Name) finished execution."
} # END Add-PowerCLI
Function Write-ColorText {
#  © Copyright 2015-2016 by Nate Stovall (nate@virtualN8.com)
# This software is released for copy, use, and modification under the GPLv3.
	[CmdletBinding()]
	Param(
		[Parameter( ValueFromPipeline=$TRUE)] [String[]]$Output,
		[Parameter()] [Alias("fg","foreground")] [System.ConsoleColor] $ForegroundColor = $Host.UI.RawUI.ForegroundColor,
		[Parameter()] [Alias("bg","background")] [System.ConsoleColor] $BackgroundColor = $Host.UI.RawUI.BackgroundColor
		)
	#Black, DarkBlue, DarkGreen, DarkCyan, DarkRed, DarkMagenta, DarkYellow, Gray, DarkGray, Blue, Green, Cyan, Red, Magenta, Yellow, White
	Write-Verbose "$(Get-Date -DisplayHint DateTime)   $($MyInvocation.MyCommand.Name) started execution."
	$DefaultFGColor = $Host.UI.RawUI.ForegroundColor
	$DefaultBGColor = $Host.UI.RawUI.BackgroundColor
	$Host.UI.RawUI.ForegroundColor = $ForegroundColor
	$Host.UI.RawUI.BackgroundColor = $BackgroundColor
	Write-Output $Output
	$Host.UI.RawUI.ForegroundColor = $DefaultFGColor
	$Host.UI.RawUI.BackgroundColor = $DefaultBGColor
	Write-Verbose "$(Get-Date -DisplayHint DateTime)   $($MyInvocation.MyCommand.Name) finished execution."
} #END Function Write-ColorText
#
#----------------------------------------------------------- VARIABLES ---#####
#
# $a=$b=$c=$NULL
#
#------------------------------------------------------ HTML VARIABLES --------
#
#------------------------------------------------------- INITILIZATION ---#####
#Set-StrictMode -Version Latest
#$ErrorActionPreference = 'SilentlyContinue'
#$VerbosePreference = 'SilentlyContinue'
#$Error.Clear();
Write-Verbose "$(Get-Date -DisplayHint DateTime)   $($MyInvocation.MyCommand.Name) started execution."
Add-PowerCLI
#---------------------------------------------------------------- MAIN ---#####
#


#--------------------------------------------------------- TERMINATION ---#####
Write-Verbose "$(Get-Date -DisplayHint DateTime)   $($MyInvocation.MyCommand.Name) finished execution." 
Exit 0
#--------------------------------------------------------------- NOTES ---#####
#
#$Error[0].Exception.GetType().FullName
#$Error[0].Exception.Message
#$Error[0].ErrorDetails.Message
#$Error[0].CategoryInfo.Reason
#If ($MyInvocation.ScriptName){ Write-Output "This was invoked via script." }
#$Obj = New-Object System.Object
#$Obj | Add-Member -type NoteProperty -Name "" -Value ""
#
#----------------------------------------------- GNU Public License v3 ---#####
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software 
# Foundation, either version 3 of the License, or (at your option) any later 
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with 
# this program.  If not, see <http://www.gnu.org/licenses/>.
#----------------------------------------------------------------- FIN ---#####